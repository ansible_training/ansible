FROM ubuntu:18.04

RUN apt-get update \
    && apt-get install -y openssh-server sshpass python3-pip python3-dev sudo \
        net-tools python-psycopg2 \
    && cd /usr/local/bin \
    && ln -s /usr/bin/python3 python \
    && pip3 install --upgrade pip

RUN mkdir -p /var/run/sshd

# authorize SSH connection with root account
RUN sed -i s'/^#PermitRootLogin .*/PermitRootLogin yes/' /etc/ssh/sshd_config

# change password root
RUN echo "root:xxx"|chpasswd

COPY entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh
ENTRYPOINT ["/entrypoint.sh"]

CMD ["/bin/bash"]